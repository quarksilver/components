import { configure, addParameters, addDecorator } from '@storybook/html';
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs';
import quarksilverTheme, { palette } from './quarksilverTheme';

// Polyfill for webcomponents support
import '@webcomponents/webcomponentsjs/webcomponents-bundle';

addParameters({
  backgrounds: [
    {
      name: 'light-bg',
      value: palette.main['700'].value,
      default: true
    },
    {
      name: 'dark-bg',
      value: palette.main['100'].value
    }
  ],
  options: {
    theme: quarksilverTheme.light
  }
});

addDecorator(withA11y);
addDecorator(withKnobs);

// automatically import all files ending in *.stories.js
const req = require.context('../stories/', true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
