const { resolve } = require('path');

// Append to Storybook's webpack configuration
module.exports = async = ({ config }) => {
  // Helpful aliases for navigating and module invocation
  config.module.rules.push(
    // For storysource addon
    {
      test: /\.stories\.js$/,
      loaders: [require.resolve('@storybook/addon-storysource/loader')],
      enforce: 'pre'
    }
  );

  return config;
};
