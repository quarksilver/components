import { create } from '@storybook/theming';
import quarks from '@quarksilver/core';

const { triadic } = quarks.colors.scheme;
const { tokenize } = quarks.toolkit;

// Theme
const base = '#348ec9';

const colors = {
  base,
  options: { range: 3 }
};

const fonts = {
  system: {
    name: 'System (Sans)',
    stack:
      '-apple-system, BlinkMacSystemFont, avenir next, avenir, helvetica neue, helvetica, Ubuntu, roboto, noto, segoe ui, arial, sans-serif',
    styles: [400, '400i', 700]
  }
};

export const palette = triadic(colors);
export const content = tokenize.fonts(fonts);

const theme = {
  light: {
    primary: palette.main['400'].value,
    secondary: palette.accent['400'].value,

    background: palette.main['700'].value,
    border: palette.main['400'].value,

    borderRadius: 2,
    font: content.system.value,

    foreground: palette.main['100'].value,
    inverse: palette.main['700'].value
  },
  dark: {
    primary: palette.main['600'].value,
    secondary: palette.accent['500'].value,

    background: palette.main['100'].value,
    border: palette.main['500'].value,

    borderRadius: 2,
    font: content.system.value,

    foreground: palette.main['700'].value,
    inverse: palette.main['700'].value
  }
};

// Light theme
const light = create({
  base: 'light',

  colorPrimary: theme.light.primary,
  colorSecondary: theme.light.secondary,

  // UI
  appBg: theme.light.background,
  appContentBg: theme.light.background,
  appBorderColor: theme.light.border,
  appBorderRadius: theme.borderRadius,

  // Typography
  fontBase: theme.font,
  fontCode: 'monospace',

  // Text colors
  textColor: theme.light.foreground,
  textInverseColor: theme.light.inverse,

  // Toolbar default and active colors
  barTextColor: theme.light.foreground,
  barBg: theme.light.background,

  brandTitle: 'Quarksilver: Demo',
  brandUrl: 'https://gitlab.com/quarksilver/components',
  brandImage: '/assets/images/logo-fullcolor-with-text.svg'
});

// Dark theme
const dark = create({
  base: 'dark',

  colorPrimary: theme.dark.primary,
  colorSecondary: theme.dark.secondary,

  // UI
  appBg: theme.dark.background,
  appContentBg: theme.dark.background,
  appBorderColor: theme.dark.border,
  appBorderRadius: theme.borderRadius,

  // Typography
  fontBase: theme.font,
  fontCode: 'monospace',

  // Text colors
  textColor: theme.dark.foreground,
  textInverseColor: theme.dark.inverse,

  // Toolbar default and active colors
  barTextColor: theme.dark.foreground,
  barBg: theme.dark.background,

  brandTitle: 'Quarksilver: Demo',
  brandUrl: 'https://gitlab.com/quarksilver/components',
  brandImage: '/assets/images/logo-fullcolor-with-text.svg'
});

export default {
  light,
  dark
};
