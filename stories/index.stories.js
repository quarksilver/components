import { storiesOf } from '@storybook/html';

import '../src';

storiesOf('Components|Colors/<qui-swatches>', module)
  .add('default', () => {
    const el = document.createElement('qui-swatches');
    el.swatchData = {
      navy: '#001f3f',
      blue: '#0074d9',
      aqua: '#7fdbff',
      teal: '#39cccc',
      olive: '#3d9970',
      green: '#2ecc40',
      lime: '#01ff70',
      yellow: '#ffdc00',

      orange: '#ff851b',
      red: '#ff4136',
      maroon: '#85144b',
      fuchsia: '#f012be',
      purple: '#b10dc9',
      black: '#111111',
      gray: '#aaaaaa',
      silver: '#dddddd',

      white: '#ffffff'
    };
    return el;
  })
  .add('<qui-swatches with-controls>', () => {
    const el = document.createElement('qui-swatches');
    el.swatchData = {
      red: '#ff4136',
      green: '#2ecc40',
      blue: '#0074d9'
    };
    el.withControls = true;
    return el;
  });

storiesOf('Components|Colors/<qui-palette>', module)
  .add('default', () => {
    const el = document.createElement('qui-palette');
    el.paletteData = {
      teal: {
        base: '#39cccc'
      },
      orange: {
        base: '#ff851b',
        options: {
          range: 3,
          mode: 'lab'
        }
      }
    };

    return el;
  })
  .add('<qui-palette with-controls>', () => {
    const el = document.createElement('qui-palette');
    el.paletteData = {
      main: { base: '#ff4136' }
    };
    el.withControls = true;
    return el;
  });

storiesOf('Components|Colors/<qui-scheme>', module)
  .add('default', () => {
    const el = document.createElement('qui-scheme');
    el.schemeConfig = {
      base: '#ff4136',
      scheme: 'monochromatic',
      options: {}
    };

    return el;
  })
  .add('<qui-scheme with-controls>', () => {
    const el = document.createElement('qui-scheme');
    el.schemeConfig = {
      base: '#ff4136',
      scheme: 'monochromatic',
      options: {}
    };

    el.withControls = true;
    return el;
  });

storiesOf('Components|Content/<qui-fonts>', module)
  .add('default', () => {
    const el = document.createElement('qui-fonts');
    el.fontData = {
      'system-sans': {
        name: 'System (Sans)',
        stack:
          '-apple-system, BlinkMacSystemFont, avenir next, avenir, helvetica neue, helvetica, Ubuntu, roboto, noto, segoe ui, arial, sans-serif',
        styles: [400, '400i', 700]
      },
      'system-serif': {
        name: 'System (Serif)',
        stack:
          'Iowan Old Style, Apple Garamond, Baskerville, Times New Roman, Droid Serif, Times, Source Serif Pro, serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol',
        styles: [400, '400i', 700]
      },
      'system-mono': {
        name: 'System (Mono)',
        stack:
          'Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace',
        styles: [400, '400i', 700]
      }
    };

    return el;
  })
  .add('<qui-fonts with-controls>', () => {
    const el = document.createElement('qui-fonts');
    el.fontData = {
      body: {
        name: 'Body',
        stack: 'sans-serif',
        styles: [400, '400i', 700]
      }
    };

    el.withControls = true;
    return el;
  });

storiesOf('Components|Content/<qui-scale>', module)
  .add('default', () => {
    const el = document.createElement('qui-scale');
    el.scaleConfig = {
      base: '1em',
      ratio: 1.25,
      limit: 9
    };

    return el;
  })
  .add('<qui-scale with-controls>', () => {
    const el = document.createElement('qui-scale');
    el.scaleConfig = {
      base: '1em',
      ratio: 1.25,
      limit: 4
    };

    el.withControls = true;
    return el;
  });
