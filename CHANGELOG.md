# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
Table of Contents

- [v0.2.2](#v022)
    - [Changed](#changed)
    - [Removed](#removed)
    - [Fixed](#fixed)
- [v0.2.1](#v021)
    - [Fixed](#fixed-1)
- [Under Development](#under-development)
- [v0.2.0](#v020)
    - [Added](#added)
    - [Changed](#changed-1)
- [v0.1.0](#v010)
    - [Added](#added-1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



## v0.2.2

### Changed

+ custom palette options now included in the main form for `<qui-palette>`
+ component buttons updated to be more semantically correct

### Removed

+ unneeded internal components
+ unused code

### Fixed

+ missing default mode value from `<qui-palette>`
+ overwriting of custom palette options from regenerating
+ `<qui-scale>` ratio is now `<input type="number">`

## v0.2.1

### Fixed

+ missing buttons from `<qui-scheme>`

## Under Development

+ Creating live demos for the documentation instead of screenshots
+ Adding more theme customization
+ `<qui-spacing>` development

## v0.2.0

### Added

+ Quarksilver components are now responsive and themeable! Check out the docs for details.

### Changed

+ All the components have undergone some design tweaks for a bit more consistency

## v0.1.0

Initial release.

### Added

+ `<qui-swatches>` component
+ `<qui-palette>` component
+ `<qui-scheme>` component
+ `<qui-fonts>` component
+ `<qui-scale>` component