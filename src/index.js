import '@webcomponents/webcomponentsjs/webcomponents-bundle.js';
import { define } from 'hybrids';

// import public components
import Swatches from './components/qui-swatches';
import Palette from './components/qui-palette';
import Scheme from './components/qui-scheme';
import Fonts from './components/qui-fonts';
import Scale from './components/qui-scale';

// Define as custom elements
define('qui-swatches', Swatches);
define('qui-palette', Palette);
define('qui-scheme', Scheme);
define('qui-fonts', Fonts);
define('qui-scale', Scale);
