import { html } from 'hybrids';

import host from '../common/host';

export default {
  data: {},
  render: ({ data }) => {
    const { name, stack, styles } = data;
    return html`
      ${host}
      <style>
        .info {
          border-bottom: 0.25em solid;
          font-size: 1.5em;
          padding-bottom: 0.5em;
        }

        .styles {
          display: flex;
          flex-wrap: wrap;
          font-size: 1.25em;
          list-style: none;
          padding: 0;
          margin: 0.5em 0;
        }

        .styles li {
          flex-basis: 33.33333334%;
          padding: 0.25em 0;
        }

        .stack {
          display: block;
          font-size: 1.5em;
          line-height: 1.5;
          margin-bottom: 0.5em;
        }

        .samples {
          display: flex;
          flex-flow: row wrap;
        }

        .sample {
          flex-basis: 100%;
          line-height: 1.5;
          font-size: 1.5em;
          margin: 0;
          padding: 0.5em;
        }

        .sample.light {
          border: 0.15em solid;
        }

        .sample.midtone,
        .sample.dark {
          color: #ffffff;
        }

        .sample.midtone {
          background: #aaaaaa;
        }

        .sample.dark {
          background: #111111;
        }

        @media (min-width: 33em) {
          .sample {
            font-size: 1.75em;
          }
        }
        @media (min-width: 53em) {
          .info {
            font-size: 1.75em;
          }
          .styles {
            font-size: 1.5em;
          }

          .styles li {
            flex-basis: 25%;
          }
          .stack {
            font-size: 1.75em;
            line-height: 1.5;
          }
          .sample {
            font-size: 2em;
            flex-basis: 50%;
            flex-grow: 1;
          }
        }
        @media (min-width: 80em) {
          .info {
            font-size: 2em;
          }
          .styles {
            font-size: 1.75em;
          }

          .styles li {
            flex-basis: 20%;
          }
          .stack {
            font-size: 2em;
            line-height: 1.5;
          }
          .sample {
            font-size: 2.25em;
            flex-basis: 50%;
            flex-grow: 1;
          }
        }
      </style>
      <section class="category" style="font-family: ${stack};">
        <div class="info">
          <span class="name">${name}</span>
        </div>
        <ul class="styles">
          ${styles.map(
            s =>
              html`
                ${typeof s === 'string' && s.match(/[italic]+/g)
                  ? html`
                      <li
                        style="font-style: italic; font-weight: ${s.replace(
                          /\D+/g,
                          ''
                        )}"
                      >
                        ${s}
                      </li>
                    `
                  : html`
                      <li style="font-weight: ${s}">${s}</li>
                    `}
              `
          )}
        </ul>
        <code class="stack">font-family: ${stack};</code>
        <div class="samples">
          <p class="sample light">
            The quick brown fox jumps over the lazy dog.
          </p>
          <p class="sample midtone">
            The quick brown fox jumps over the lazy dog.
          </p>
          <p class="sample dark">
            The quick brown fox jumps over the lazy dog.
          </p>
        </div>
      </section>
    `;
  }
};
