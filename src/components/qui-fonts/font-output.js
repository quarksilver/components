import { html, parent } from 'hybrids';

import QSTypeface from '../qs-typeface';
import FontStore from './font-store';

import host from '../common/host';

export default {
  store: parent(FontStore),
  render: ({ store: { fonts } }) =>
    html`
      ${host}
      <style>
        qs-typeface {
          margin-bottom: 3em;
        }
      </style>
      ${Object.keys(fonts).map(
        key =>
          html`
            <qs-typeface data="${fonts[key]}"></qs-typeface>
          `
      )}
    `.define({ QSTypeface })
};
