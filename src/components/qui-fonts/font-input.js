import { html, parent } from 'hybrids';

import FontStore from './font-store';

import theme from '../common/input-theme';

const change = host => {
  host.store.fonts = {
    ...host.store.fonts,
    [host.name
      .replace(/([()\[\]:])+/g, '')
      .replace(/([\s])+/g, '-')
      .toLowerCase()]: {
      name: host.name,
      stack: host.stack,
      styles: host.styles
        .split(', ')
        .map(str => (str.match(/[italic]+/g) ? str : parseInt(str)))
    }
  };
};

const clear = host => {
  event.preventDefault();

  host.store.fonts = {};
};

export default {
  store: parent(FontStore),
  name: 'Body',
  stack: 'sans-serif',
  styles: '400, 400i, 700',
  render: ({ name, stack, styles }) =>
    html`
      ${theme}
      <style>
        label {
          line-height: 0.8;
        }
        @media (min-width: 33em) {
          label {
            display: block;
            line-height: 1.25;
            height: 2.5em;
          }
        }
        @media (min-width: 53em) {
          label {
            display: block;
            line-height: 1.25;
            height: 2em;
          }
        }
        @media (min-width: 63em) {
          .field {
            flex-basis: 32.5%;
          }

          label {
            height: 1.5em;
            line-height: 0.9;
          }
          small {
            display: inline;
          }
        }
      </style>
      <form action="">
        <div class="field name">
          <label for="name" id="nameLabel"
            >Name<small
              >(alphanumeric chars allowed, also '[]', '()', ':')</small
            ></label
          >
          <input
            name="name"
            type="text"
            aria-labelledby="nameLabel"
            value="${name}"
            oninput="${html.set('name')}"
          />
        </div>
        <div class="field styles">
          <label for="styles" id="stylesLabel"
            >Styles<small>(comma separated)</small></label
          >
          <input
            name="styles"
            type="text"
            aria-labelledby="stylesLabel"
            value="${styles}"
            oninput="${html.set('styles')}"
          />
        </div>
        <div class="field stack">
          <label for="stack" id="stackLabel"
            >Stack<small>(comma separated)</small></label
          >
          <input
            name="stack"
            type="text"
            aria-labelledby="stackLabel"
            value="${stack}"
            oninput="${html.set('stack')}"
          />
        </div>
        <div class="controls">
          <button type="button" class="add" onclick="${change}">
            Add/Update
          </button>
          <button type="button" class="clear" onclick="${clear}">
            Clear Data
          </button>
        </div>
      </form>
    `
};
