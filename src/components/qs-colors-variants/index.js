import { html } from 'hybrids';
import chroma from 'chroma-js';
import quarks from '@quarksilver/core';

import host from '../common/host';

const { tints, shades } = quarks.toolkit.colors.variants;

import QSColorsSwatch from '../qs-colors-swatch';

export default {
  color: 'rebeccapurple',
  key: '',
  options: {},
  render: ({ color, key, options }) => {
    return html`
      ${host}
      <style>
        :host {
          --swatches-per-row: 5;
          display: flex;
          flex-flow: row wrap;
        }

        .variant {
          flex-basis: calc(100% / var(--swatches-per-row));
          flex-grow: 1;
        }

        .color {
          flex-basis: 100%;
        }

        qs-colors-swatch {
          font-size: 0.6em;
        }

        @media (min-width: 39em) {
          qs-colors-swatch {
            font-size: 0.7em;
          }
        }
        @media (min-width: 54em) {
          qs-colors-swatch {
            font-size: 0.9em;
          }
        }
      </style>
      ${shades(color, options)
        .map(
          v =>
            html`
              <qs-colors-swatch
                class="variant shade"
                color="${v}"
              ></qs-colors-swatch>
            `
        )
        .reverse()}
      <qs-colors-swatch
        class="color"
        key="${key}"
        color="${color}"
      ></qs-colors-swatch>
      ${tints(color, options).map(
        v =>
          html`
            <qs-colors-swatch
              class="variant tint"
              color="${v}"
            ></qs-colors-swatch>
          `
      )}
    `.define({ QSColorsSwatch });
  }
};
