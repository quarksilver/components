import { html, parent } from 'hybrids';

import QSColorsSwatch from '../qs-colors-swatch';
import SwatchStore from './swatch-store';

import host from '../common/host';

export default {
  store: parent(SwatchStore),
  render: ({ store: { swatches } }) =>
    html`
      ${host}
      <style>
        :host {
          --swatches-per-row: 5;
          display: flex;
          flex-flow: row wrap;
        }
        qs-colors-swatch {
          flex-basis: calc(100% / var(--swatches-per-row));
          flex-grow: 1;
          font-size: 0.6em;
        }

        @media (min-width: 39em) {
          qs-colors-swatch {
            font-size: 0.7em;
          }
        }
        @media (min-width: 54em) {
          qs-colors-swatch {
            font-size: 0.9em;
          }
        }
      </style>
      ${Object.keys(swatches).map(key => {
        const value = swatches[key];
        return html`
          <qs-colors-swatch color="${value}" key="${key}"></qs-colors-swatch>
        `;
      })}
    `.define({ QSColorsSwatch })
};
