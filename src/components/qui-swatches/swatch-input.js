import { html, parent } from 'hybrids';

import SwatchStore from './swatch-store';

import theme from '../common/input-theme';

const change = host => {
  host.store.swatches = {
    ...host.store.swatches,
    ...{ [host.name]: host.color }
  };
};

const clear = host => {
  host.store.swatches = {};
};

export default {
  store: parent(SwatchStore),
  name: 'red',
  color: '#ff4136',
  render: ({ name, color }) =>
    html`
      ${theme}
      <form action="">
        <div class="field name">
          <label id="nameLabel" for="name"
            >Name<small>(separate words with '-')</small></label
          >
          <input
            type="text"
            aria-labelledby="nameLabel"
            name="name"
            value="${name}"
            oninput="${html.set('name')}"
          />
        </div>
        <div class="field color">
          <label id="colorLabel" for="color"
            >Color<small>(any valid CSS color)</small></label
          >
          <input
            type="color"
            aria-labelledby="colorLabel"
            name="color"
            value="${color}"
            oninput="${html.set('color')}"
          />
        </div>
        <div class="controls">
          <button type="button" class="add" onclick="${change}">
            Add/Update
          </button>
          <button type="button" class="clear" onclick="${clear}">
            Clear Data
          </button>
        </div>
      </form>
    `
};
