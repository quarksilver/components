import { html } from 'hybrids';

export default html`
  <style>
    :host {
      --form-flow: row wrap;
      --form-font: sans-serif;
      --form-margin: 1em;
      --form-fg: #111111;

      --label-fg: var(--form-fg);
      --label-size: 1.25em;
      --label-spacing: 0.5em;

      --details-fg: #aaaaaa;
      --details-spacing: 0.5em;
      --details-size: 0.5em;

      --input-border: 0.25em solid currentColor;
      --input-size: 1em;
      --input-padding: 0.5em;
      --input-bg: transparent;
      --input-fg: var(--form-fg);
      --input-radius: 1em;

      --button-border: var(--input-border);
      --button-size: var(--input-size);
      --button-padding: var(--input-padding);
      --button-bg: var(--input-bg);
      --button-fg: var(--form-fg);
      --button-radius: var(--input-radius);
    }

    form {
      display: flex;
      color: var(--form-fg);
      flex-flow: var(--form-flow);
      font-family: var(--form-font);
      margin-bottom: var(--form-margin);
    }

    label {
      color: var(--label-fg);
      display: block;
      font-size: var(--label-size);
      margin-bottom: var(--label-spacing);
    }

    small {
      color: var(--details-fg);
      font-size: var(--details-size);
      margin-left: var(--details-spacing);
    }

    input[type='text'],
    input[type='number'],
    input[type='color'] {
      border: var(--input-border);
      border-radius: var(--input-radius);
      color: var(--input-fg);
      font-size: var(--input-size);
      width: 100%;
    }

    input[type='text'],
    input[type='number'] {
      background: var(--input-bg);
      padding: var(--input-padding);
    }

    input[type='color'] {
      padding: 0;
      height: 2.75em;
    }

    button {
      background: var(--button-bg);
      color: var(--button-fg);
      border: var(--button-border);
      border-radius: var(--button-radius);
      font-size: var(--button-size);
      padding: var(--button-padding);
    }
  </style>
`;
