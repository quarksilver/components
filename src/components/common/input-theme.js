import { html } from 'hybrids';

import host from './host';
import form from './form';

export default html`
  ${host} ${form}
  <style>
    /** Themeability */
    :host {
      /** element theming */
      --controls-fg: #111111;
      --controls-bg: transparent;

      /** overall form style */
      --controls-form-flow: row wrap;
      --controls-form-font: sans-serif;
      --controls-form-margin: 1em;
      --controls-form-fg: inherit;

      /** label theming */
      --controls-label-fg: var(--controls-form-fg, inherit);
      --controls-label-size: 2em;
      --controls-label-spacing: 0.5em;

      /** details theming */
      --controls-details-fg: #aaaaaa;
      --controls-details-size: 0.5em;

      /** input theming */
      --controls-input-border: 0.25em solid currentColor;
      --controls-input-size: 1em;
      --controls-input-padding: 0.5em;
      --controls-input-bg: transparent;
      --controls-input-fg: var(--controls-form-fg);
      --controls-input-radius: 1em;

      /** button theming */
      --controls-button-border: var(--controls-input-border);
      --controls-button-size: var(--controls-input-size);
      --controls-button-padding: var(--controls-input-padding);
      --controls-button-bg: var(--controls-input-bg);
      --controls-button-fg: var(--controls-input-fg);
      --controls-button-radius: var(--controls-input-radius);

      background: var(--controls-bg);
      color: var(--controls-fg);
    }

    form {
      --form-flow: var(--controls-form-flow);
      --form-font: var(--controls-form-font);
      --form-margin: var(--controls-form-margin);
      --form-fg: var(--controls-form-fg);
    }

    label {
      --label-fg: var(--controls-label-fg);
      --label-spacing: var(--controls-label-spacing);
      --label-size: var(--controls-label-size);
    }

    small {
      --details-fg: var(--controls-details-fg);
      --details-spacing: var(--controls-label-spacing);
      --details-size: var(--controls-details-size);
    }

    input[type='text'],
    input[type='number'],
    input[type='color'] {
      --input-fg: var(--controls-input-fg);
      --input-radius: var(--controls-input-radius);
      --input-border: var(--controls-input-border);
      --input-size: var(--controls-input-size);
    }

    input[type='text'],
    input[type='number'] {
      --input-bg: var(--controls-input-bg);
      --input-padding: var(--controls-input-padding);
    }

    button {
      --button-bg: var(--controls-button-bg);
      --button-fg: var(--controls-button-fg);
      --button-radius: var(--controls-button-radius);
      --button-border: var(--controls-button-border);
      --button-size: var(--controls-button-size);
      --button-padding: var(--controls-button-padding);
    }

    .field {
      flex-basis: 100%;
      margin: 0 0 1em 0;
    }

    .controls {
      display: flex;
      flex-flow: row wrap;
      flex-basis: 100%;
    }

    .controls button {
      flex-basis: 100%;
      margin: 0.5em 0 0 0;
    }

    @media (min-width: 28em) {
      .controls {
        justify-content: space-around;
      }
      .controls button {
        flex-basis: 48%;
      }
    }

    @media (min-width: 33em) {
      :host {
        font-size: 1.25em;
      }
      form {
        justify-content: space-between;
      }

      small {
        display: block;
        margin-left: 0;
        margin-top: 0.25em;
      }

      .field {
        flex-basis: 49%;
      }
    }

    @media (min-width: 50em) {
      :host {
        font-size: 1.5em;
      }
    }

    @media (min-width: 63em) {
      small {
        display: inline-block;
        margin-left: 0.5em;
        margin-top: 0.25em;
      }

      .controls {
        justify-content: flex-end;
      }

      .controls button {
        flex-basis: 20%;
        margin-left: 1em;
      }

      .add {
        font-weight: 700;
      }
    }
  </style>
`;
