import { html, parent } from 'hybrids';

import QSColorsVariants from '../qs-colors-variants';
import PaletteStore from './palette-store';

import host from '../common/host';

export default {
  store: parent(PaletteStore),
  render: ({ store: { palette } }) =>
    html`
      ${host}
      <style>
        :host {
          --variants-per-row: 5;
        }
        qs-colors-variants {
          --swatches-per-row: var(--variants-per-row);
        }
      </style>
      ${Object.keys(palette).map(category => {
        const { base, options } = palette[category];
        return html`
          <qs-colors-variants
            color="${base}"
            key="${category}"
            options="${options}"
          ></qs-colors-variants>
        `;
      })}
    `.define({ QSColorsVariants })
};
