import { html } from 'hybrids';

import ScaleStore from './scale-store';
import ScaleInput from './scale-input';
import ScaleOutput from './scale-output';

import host from '../common/host';

export default {
  scaleConfig: {},
  withControls: false,
  render: ({ scaleConfig, withControls }) => {
    return html`
      ${host}
      <style>
        :host {
          --theme-controls-fg: #111111;
          --theme-controls-bg: transparent;

          --theme-form-flow: row wrap;
          --theme-form-font: sans-serif;
          --theme-form-margin: 1em;
          --theme-form-fg: inherit;

          --theme-label-fg: var(--theme-form-fg, inherit);
          --theme-label-size: 1.5em;
          --theme-label-spacing: 0.5em;

          --theme-details-fg: #aaaaaa;
          --theme-details-size: 0.5em;

          --theme-input-border-fg: var(--theme-form-fg, inherit);

          --theme-input-border: 0.25em solid currentColor;
          --theme-input-size: 1em;
          --theme-input-padding: 0.5em;
          --theme-input-bg: transparent;
          --theme-input-fg: var(--theme-form-fg);
          --theme-input-radius: 0.3em;

          --theme-button-border: var(--theme-input-border);
          --theme-button-size: var(--theme-input-size);
          --theme-button-padding: var(--theme-input-padding);
          --theme-button-bg: var(--theme-input-bg);
          --theme-button-fg: var(--theme-input-fg);
          --theme-button-radius: var(--theme-input-radius);

          --output-variants-per-row: 3;
        }

        scale-input {
          --controls-fg: var(--theme-controls-fg);
          --controls-bg: var(--theme-controls-bg);

          --controls-form-flow: var(--theme-form-flow);
          --controls-form-font: var(--theme-form-font);
          --controls-form-margin: var(--theme-form-margin);
          --controls-form-fg: var(--theme-form-fg);

          --controls-label-fg: var(--theme-label-fg);
          --controls-label-size: var(--theme-label-size);
          --controls-label-spacing: var(--theme-label-spacing);

          --controls-details-fg: var(--theme-details-fg);
          --controls-details-size: var(--theme-details-size);

          --controls-input-border: var(--theme-input-border);
          --controls-input-size: var(--theme-input-size);
          --controls-input-bg: var(--theme-input-bg);
          --controls-input-fg: var(--theme-input-fg);
          --controls-input-padding: var(--theme-input-padding);
          --controls-input-radius: var(--theme-input-radius);

          --controls-button-border: var(--theme-button-border);
          --controls-button-size: var(--theme-button-size);
          --controls-button-bg: var(--theme-button-bg);
          --controls-button-fg: var(--theme-button-fg);
          --controls-button-padding: var(--theme-button-padding);
          --controls-button-radius: var(--theme-button-radius);
        }
      </style>
      ${withControls
        ? html`<scale-store config="${scaleConfig}"><scale-input></scale-input><scale-output></scale-output</scale-store>`
        : html`<scale-store config="${scaleConfig}"><scale-output></scale-output</scale-store>`}
    `.define({ ScaleStore, ScaleInput, ScaleOutput });
  }
};
