import { html, parent, property } from 'hybrids';

import ScaleStore from './scale-store';

import theme from '../common/input-theme';

const change = host => {
  host.store.config = {
    ...host.store.config,
    ...{
      base: host.base,
      ratio: host.ratio,
      limit: host.limit
    }
  };
};

const clear = host => {
  host.store.config = {};
};

export default {
  store: parent(ScaleStore),
  base: '1em',
  ratio: 1.25,
  limit: 4,
  render: ({ base, ratio, limit }) =>
    html`
      ${theme}
    <style>

        label {
          line-height: 0.8;
        }
        @media (min-width: 33em) {
          label {
            display: block;
            line-height: 1.25;
            height: 2.5em;
          }
        }
        @media (min-width: 53em) {
          label {
            display: block;
            line-height: 1.25;
            height: 2em;
          }
        }
        @media (min-width: 63em) {
          .field {
            flex-basis: 32.5%;
          }

          label {
            height: 1.5em;
            line-height: 0.9;
          }
          small {
            display: inline;
          }
        }
    </style>
      <form action="">
        <div class="field base">
          <label for="base" id="baseLabel"
            >Base<small>(accepts absolute and relative units)</small></label
          >
          <input
            name="base"
            type="text"
            aria-labelledby="baseLabel"
            value="${base}"
            oninput="${html.set('base')}"
          />
        </div>
        <div class="field ratio">
          <label for="ratio" id="ratioLabel"
            >Ratio<small>(must be unitless)</small></label
          >
          <input
            name="ratio"
            type="number"
            aria-labelledby="ratioLabel"
            value="${ratio}"
            min="1.067"
            step="0.001"
            oninput="${html.set('ratio')}"
          />
        </div>
        <div class="field limit">
          <label for="limit" id="limitLabel">Limit<small>(lower: 4, upper: 8)</small></label>
          <input
            name="ratio"
            type="number"
            aria-labelledby="limitLabel"
            min="4"
            max="8"
            value="${limit}"
            oninput="${html.set('limit')}"
          />
        </div>
        <div class="controls">
          <button type="button" class="add" onclick="${change}">
            Generate/Update
          </button>
          <button type="button" class="clear" onclick="${clear}">
            Clear Data
          </button>
      </form>
    `
};
