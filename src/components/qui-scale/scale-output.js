import { html, parent } from 'hybrids';
import quarks from '@quarksilver/core';

import ScaleStore from './scale-store';

import host from '../common/host';

const { content } = quarks.toolkit;

export default {
  store: parent(ScaleStore),
  sampleContent: 'The quick brown fox jumps over the lazy dog.',
  render: ({ sampleContent, store: { config } }) =>
    html`
      ${host}
      <style>
        .container,
        .size {
          display: flex;
          flex-direction: column;
          font-family: sans-serif;
        }

        .scale {
          list-style: none;
          margin: 0;
          padding: 0;
        }

        .size {
          margin-top: 1em;
        }

        .x,
        .value {
          display: inline-block;
          padding: 0.5em;
        }

        .x {
          background: #111111;
          color: #ffffff;
          font-size: 1em;
        }

        .value {
          display: inline-block;
          font-size: 1.25em;
          padding: 0.5em;
        }

        .sample {
          border: 8px solid;
          line-height: 1.5;
          margin: 0;
          padding: 0.5em;
        }

        @media (min-width: 38em) {
          .x {
            font-size: 1.25em;
          }

          .value {
            font-size: 1.5em;
          }
        }
        @media (min-width: 63em) {
          .x {
            font-size: 1.5em;
          }

          .value {
            font-size: 1.75em;
          }
        }
        @media (min-width: 80em) {
          .x {
            font-size: 1.75em;
          }

          .value {
            font-size: 2em;
          }
        }
      </style>
      <section class="container">
        <ul class="scale fs">
          ${content.scale(config).map((value, i) => {
            return html`
              <li class="size">
                <div class="info">
                  <span class="x">${++i}x</span>
                  <code class="value">${value}</code>
                </div>
                <p class="sample" style="font-size: ${value}">
                  ${sampleContent}
                </p>
              </li>
            `;
          })}
        </ul>
      </section>
    `
};
