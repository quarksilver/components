import { html } from 'hybrids';
import chroma from 'chroma-js';

import host from '../common/host';

function colorHasKey(color, key) {
  const base = chroma(color).hex();
  return key
    ? html`
        <div class="swatch">
          <code class="value">${key}: ${base}</code>
        </div>
      `
    : html`
        <div class="swatch">
          <code class="value">${base}</code>
        </div>
      `;
}

export default {
  color: 'rebeccapurple',
  key: '',
  render: ({ color, key }) => {
    return html`
      ${host}
      <style>
        .swatch {
          background: ${color};
          flex-basis: 100%;
          padding: 2em;
        }

        .value {
          background: hsla(0, 100%, 100%, 0.8);
          display: block;
          font-size: 1.75em;
          padding: 0.5em;
          text-align: center;
        }
      </style>
      ${colorHasKey(color, key)}
    `;
  }
};
