import { html, parent } from 'hybrids';

import SchemeStore from './scheme-store';

import theme from '../common/input-theme';

const change = host => {
  host.store.data = {
    ...host.store.data,
    ...{
      base: host.base,
      scheme: host.scheme,
      options: {
        contrast: host.contrast,
        range: host.range,
        mode: host.mode
      }
    }
  };
};

const clear = host => {
  host.store.data = {};
};
export default {
  store: parent(SchemeStore),
  base: '#ff4136',
  scheme: 'monochromatic',
  contrast: 95,
  range: 4,
  mode: 'lab',
  render: ({ base, scheme, contrast, range, mode }) =>
    html`
      ${theme}
      <style>
        label {
          line-height: 0.8;
        }
        @media (min-width: 33em) {
          label {
            display: block;
            line-height: 1.25;
            height: 3em;
          }
        }
        @media (min-width: 63em) {
          .contrast.field,
          .range.field,
          .mode.field {
            flex-basis: 33.33333334%;
          }

          label {
            height: 2em;
          }
          small {
            display: inline;
          }
        }
      </style>
      <form action="">
        <div class="field scheme">
          <label for="scheme" id="schemeLabel"
            >Scheme<small
              >(monochromatic, complementary, split complement, triadic, clash,
              analogous, tetradic, square)</small
            ></label
          >
          <input
            name="scheme"
            aria-labelledby="schemeLabel"
            type="text"
            value="${scheme}"
            oninput="${html.set('scheme')}"
          />
        </div>
        <div class="field base">
          <label for="base" id="baseLabel"
            >Base<small>(what color to generate from)</small></label
          >
          <input
            name="base"
            aria-labelledby="baseLabel"
            type="color"
            value="${base}"
            oninput="${html.set('base')}"
          />
        </div>
        <div class="field contrast">
          <label for="contrast">Contrast</label>
          <input
            name="contrast"
            type="range"
            min="30"
            max="95"
            step="5"
            value="${contrast}"
            oninput="${html.set('contrast')}"
          />
        </div>
        <div class="field range">
          <label for="range">Range<small>(number of variants)</small></label>
          <input
            name="range"
            type="range"
            min="1"
            max="10"
            value="${range}"
            oninput="${html.set('range')}"
          />
        </div>
        <div class="field mode">
          <label for="mode" id="mode"
            >Mode<small
              >(color space: l(ab|ch), hs(l|i|v), (l)rgb)</small
            ></label
          >
          <input
            name="mode"
            aria-labelledby="mode"
            type="text"
            value="${mode}"
            oninput="${html.set('mode')}"
          />
        </div>
        <div class="controls">
          <button type="button" class="add" onclick="${change}">
            Generate/Update
          </button>
          <button type="button" class="clear" onclick="${clear}">
            Clear Data
          </button>
      </form>
    `
};
