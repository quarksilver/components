import { html } from 'hybrids';

import SchemeStore from './scheme-store';
import SchemeInput from './scheme-input';
import SchemeOutput from './scheme-output';

import host from '../common/host';

export default {
  schemeConfig: {},
  withControls: false,
  render: ({ schemeConfig, withControls }) => {
    return html`
      ${host}
      <style>
        :host {
          --theme-controls-fg: #111111;
          --theme-controls-bg: transparent;

          --theme-form-flow: row wrap;
          --theme-form-font: sans-serif;
          --theme-form-margin: 1em;
          --theme-form-fg: inherit;

          --theme-label-fg: var(--theme-form-fg, inherit);
          --theme-label-size: 1.5em;
          --theme-label-spacing: 0.5em;

          --theme-details-fg: #aaaaaa;
          --theme-details-size: 0.5em;

          --theme-input-border-fg: var(--theme-form-fg, inherit);

          --theme-input-border: 0.25em solid currentColor;
          --theme-input-size: 1em;
          --theme-input-padding: 0.5em;
          --theme-input-bg: transparent;
          --theme-input-fg: var(--theme-form-fg);
          --theme-input-radius: 0.3em;

          --theme-button-border: var(--theme-input-border);
          --theme-button-size: var(--theme-input-size);
          --theme-button-padding: var(--theme-input-padding);
          --theme-button-bg: var(--theme-input-bg);
          --theme-button-fg: var(--theme-input-fg);
          --theme-button-radius: var(--theme-input-radius);

          --output-variants-per-row: 3;
        }

        scheme-input {
          --controls-fg: var(--theme-controls-fg);
          --controls-bg: var(--theme-controls-bg);

          --controls-form-flow: var(--theme-form-flow);
          --controls-form-font: var(--theme-form-font);
          --controls-form-margin: var(--theme-form-margin);
          --controls-form-fg: var(--theme-form-fg);

          --controls-label-fg: var(--theme-label-fg);
          --controls-label-size: var(--theme-label-size);
          --controls-label-spacing: var(--theme-label-spacing);

          --controls-details-fg: var(--theme-details-fg);
          --controls-details-size: var(--theme-details-size);

          --controls-input-border: var(--theme-input-border);
          --controls-input-size: var(--theme-input-size);
          --controls-input-bg: var(--theme-input-bg);
          --controls-input-fg: var(--theme-input-fg);
          --controls-input-padding: var(--theme-input-padding);
          --controls-input-radius: var(--theme-input-radius);

          --controls-button-border: var(--theme-button-border);
          --controls-button-size: var(--theme-button-size);
          --controls-button-bg: var(--theme-button-bg);
          --controls-button-fg: var(--theme-button-fg);
          --controls-button-padding: var(--theme-button-padding);
          --controls-button-radius: var(--theme-button-radius);
        }

        scheme-output {
          --variants-per-row: var(--output-variants-per-row);
        }
      </style>
      ${withControls
        ? html`
            <scheme-store data="${schemeConfig}">
              <scheme-input></scheme-input>
              <scheme-output></scheme-output>
            </scheme-store>
          `
        : html`
            <scheme-store data="${schemeConfig}">
              <scheme-output></scheme-output>
            </scheme-store>
          `}
    `.define({ SchemeStore, SchemeInput, SchemeOutput });
  }
};
