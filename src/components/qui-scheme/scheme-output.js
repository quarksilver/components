import { html, parent } from 'hybrids';
import quarks from '@quarksilver/core';

import QSColorsVariants from '../qs-colors-variants';
import SchemeStore from './scheme-store';

import host from '../common/host';

const { swatch, palette } = quarks.toolkit.colors;

function format(data, options) {
  if (data.length === 4)
    return html`
      <qs-colors-variants
        color="${data[0]}"
        key="main"
        options="${options}"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[1]}"
        key="accent"
        options="${options}"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[2]}"
        key="spot"
        options="${options}"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[3]}"
        key="flourish"
        options="${options}"
      ></qs-colors-variants>
    `;
  if (data.length === 3)
    return html`
      <qs-colors-variants
        color="${data[0]}"
        key="main"
        options="${options}"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[1]}"
        options="${options}"
        key="accent"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[2]}"
        options="${options}"
        key="spot"
      ></qs-colors-variants>
    `;
  if (data.length === 2)
    return html`
      <qs-colors-variants
        color="${data[0]}"
        key="main"
        options="${options}"
      ></qs-colors-variants>
      <qs-colors-variants
        color="${data[1]}"
        key="accent"
        options="${options}"
      ></qs-colors-variants>
    `;
  return html`
    <qs-colors-variants
      color="${data[0]}"
      key="main"
      options="${options}"
    ></qs-colors-variants>
  `;
}

function generate(base, options = {}, scheme = 'monochromatic') {
  switch (scheme) {
    case 'square':
      return format(palette.tetrad(base, 90), options);
    case 'tetradic':
      return format(palette.tetrad(base), options);
    case 'analogous':
      return format([base, ...palette.spread(base)], options);
    case 'clash':
      return format(palette.triad(base, 90), options);
    case 'triadic':
      return format(palette.triad(base), options);
    case 'split complement':
      return format(palette.triad(base, 150), options);
    case 'complementary':
      return format([base, swatch.complement(base)], options);
  }
  return format([base], options);
}

export default {
  store: parent(SchemeStore),
  render: ({ store: { data } }) =>
    html`
      <style>
        :host {
          --variants-per-row: 5;
        }
        qs-colors-variants {
          --swatches-per-row: var(--variants-per-row);
        }
      </style>
      ${host} ${generate(data.base, data.options, data.scheme)}
    `.define({ QSColorsVariants })
};
