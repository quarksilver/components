![Quarksilver Components Logo](assets/logo-components-with-text.png)

# Quarksilver (Components)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
Table of Contents

- [Introduction](#introduction)
- [Installation](#installation)
    - [As a Node Module/Dependency](#as-a-node-moduledependency)
    - [As a Script](#as-a-script)
- [How to Use](#how-to-use)
    - [DOM](#dom)
    - [Custom Element](#custom-element)
    - [Theming ( added in v0.2.0)](#theming-added-in-v020)
        - [DOM](#dom-1)
        - [CSS](#css)
        - [All Custom Properties and Default Values (v0.2.0)](#all-custom-properties-and-default-values-v020)
- [Included Components](#included-components)
    - [\<qui-swatches\>](#%5Cqui-swatches%5C)
        - [Initialize](#initialize)
        - [Properties](#properties)
            - [swatch-data](#swatch-data)
        - [Result](#result)
    - [\<qui-palette\>](#%5Cqui-palette%5C)
        - [Initialize](#initialize-1)
        - [Properties](#properties-1)
            - [palette-data](#palette-data)
        - [Result](#result-1)
    - [\<qui-scheme\>](#%5Cqui-scheme%5C)
        - [Initialize](#initialize-2)
        - [Properties](#properties-2)
            - [scheme-config](#scheme-config)
        - [Result](#result-2)
    - [\<qui-fonts>](#%5Cqui-fonts)
        - [Initialize](#initialize-3)
        - [Properties](#properties-3)
            - [font-data](#font-data)
        - [Result](#result-3)
    - [\<qui-scale\>](#%5Cqui-scale%5C)
        - [Initialize](#initialize-4)
        - [Properties](#properties-4)
            - [scale-config](#scale-config)
        - [Result](#result-4)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This is the component library for the [Quarksilver design system toolkit](https://gitlab.com/quarksilver). The module exposes a set of custom elements used for building Quarksilver’s own UIs and for plugging into other interfaces. If you want to learn more about the Quarksilver project and why it exists, then you’ll want to [read the Core docs](https://gitlab.com/quarksilver/core).

As of **v0.2.0**, Quarksilver components are responsive and [customizable with theme variables](#theming-added-in-v020).

## Installation

### As a Node Module/Dependency

> To use Quarksilver components as a node module, you’ll need the latest version of Node.js LTS and NPM. Also recommended to install Yarn.

Next:

```bash
npm i @quarksilver/components

# OR

yarn add @quarksilver/components
```

```js
import '@quarksilver/components';
```

### As a Script

```html
<script src="https://unpkg.com/@quarksilver/components@0.2.1/dist-web/index.js"></script>
```

## How to Use

After using either method to install, the way you use Quarksilver components is the same way you use HTML. Either with the DOM or directly in your document.

### DOM

```js
// Query your root element
const root = document.querySelector('#root');

// Create a Quarksilver component
const swatches = document.createElement('qui-swatches');

// Initialize with data
swatches.swatchData = {
  red: '#ff4136',
  green: '#2ecc40',
  blue: '#0074d9'
};

// OPTIONAL enable controls to build data
swatches.withControls = true;

// Append the element
root.append(swatches);

// Do Stuff

// Check available data
document.querySelector('qui-swatches').swatchData;
```

### Custom Element

```html
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="withd=device-width, initial-scale=1">
    <title>Custom element demo</title>
  </head>
  <body>
    <qui-swatches with-controls></qui-swatches>
    <script src="https://unpkg.com/@quarksilver/components@0.2.1/dist-web/index.js"></script>
  </body>
</html>
```

### Theming ( added in v0.2.0)

Theming capability for all Quarksilver components landed in **v0.2.0**. If you want to try it out, be sure to upgrade the library. Naturally, the form theming only affects interface mode.

#### DOM

```css
// Get a theme custom property from an element
swatches.style.getPropertyValue('--theme-controls-fg');

// set a new value for the property on an element
swatches.style.setProperty('--theme-controls-fg', 'dodgerblue');
```

#### CSS

```css
qui-swatches {
  --theme-controls-fg: dodgerblue;
  
  /** Additional styling */
}
```

#### All Custom Properties and Default Values (v0.2.0)

```css
/** overall form styling */
--theme-controls-fg: #111111;
--theme-controls-bg: transparent;

--theme-form-flow: row wrap;
--theme-form-font: sans-serif;
--theme-form-margin: 1em;
--theme-form-fg: inherit;

--theme-label-fg: var(--theme-form-fg, inherit);
--theme-label-size: 1.5em;
--theme-label-spacing: 0.5em;

--theme-details-fg: #aaaaaa;
--theme-details-size: 0.5em;

--theme-input-border-fg: var(--theme-form-fg, inherit);

--theme-input-border: 0.25em solid currentColor;
--theme-input-size: 1em;
--theme-input-padding: 0.5em;
--theme-input-bg: transparent;
--theme-input-fg: var(--theme-form-fg);
--theme-input-radius: 0.3em;

--theme-button-border: var(--theme-input-border);
--theme-button-size: var(--theme-input-size);
--theme-button-padding: var(--theme-input-padding);
--theme-button-bg: var(--theme-input-bg);
--theme-button-fg: var(--theme-input-fg);
--theme-button-radius: var(--theme-input-radius);

/** specific to qui-swatches */
--output-swatches-per-row: 5;
/** specific to qui-palette, qui-scheme */
--output-variants-per-row: 3

```

## Included Components

All components have an `el.withControls` property that enables interface mode. Something I built in mainly to ease the use of building UIs. You can pass and update data in JavaScript just fine.

### \<qui-swatches\>

#### Initialize

```js
const swatches = document.createElement('qui-swatches')
```

#### Properties

##### swatch-data

Accepts a valid Quarksilver swatch collection.

```js
swatches.swatchData = {
  red: '#ff4136',
  green: '#2ecc40',
  blue: '#0074d9'
};
```

#### Result

![\<qui-swatches\> component screenshot](assets/qui-swatches-screen.png)

### \<qui-palette\>

#### Initialize

```js
const palette = document.createElement('qui-palette');
```

#### Properties

##### palette-data

Accepts a valid Quarksilver palette configuration.

```js
palette.paletteData = {
  red: {
    base: '#ff4136',
    // OPTIONAL
    options: {
      contrast: 95,
      range: 4,
      mode: 'lab'
    }
  }
}
```

#### Result

![\<qui-palette\> component screenshot](assets/qui-palette-screen.png)

### \<qui-scheme\>

#### Initialize

```js
const scheme = document.createElement('qui-scheme');
```

#### Properties

##### scheme-config

Accepts a valid Quarksilver scheme configuration.

```js
scheme.schemeConfig = {
  base: '#ff4136',
  scheme: 'monochromatic',
  options: {
    contrast: 95,
    range: 4,
    mode: 'lab'
  }
};
```

#### Result

![\<qui-scheme\> component screenshot](assets/qui-scheme-screen.png)



### \<qui-fonts>

#### Initialize

```js
const fonts = document.createElement('qui-fonts');
```

#### Properties

##### font-data

Accepts a valid Quarksilver font configuration.

```js
fonts.fontData = {
  body: {
    name: 'Body',
    stack: 'Roboto',
    styles: [400, '400i', 700]
  }
};
```

#### Result

![\<qui-fonts\> component screenshot](assets/qui-fonts-screen.png)

### \<qui-scale\>

#### Initialize

```js
const scale = document.createElement('qui-scale');
```

#### Properties

##### scale-config

Accepts a valid Quarksilver modular scale configuration.

```js
scale.scaleConfig = {
  base: '1em',
  ratio: 1.25,
  limit: 4
};
```

#### Result

![\<qui-scale\> component screenshot](assets/qui-scale-screen.png)

